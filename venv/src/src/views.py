from django.contrib.auth import authenticate, login, get_user_model
from django.http import HttpResponse
from django.shortcuts import render, redirect

from .forms import ContactForm


def home_page(request):
    # print(request.session.get("first_name" , "Unknown"))
    context = {
        "title":"Hello World",
        "content":"welcome to the homepage."
    
    }

    return render(request, "home_page.html", context)


def about_page(request):
    context = {
        "title":"About Page",
        "content":"welcome to the about page."
    }

    return render(request, "home_page.html", context)

def contact_page(request):
    contact_form = ContactForm(request.POST or None)
    context = {
        "title":"Hello World",
        "content":"welcome to the contactpage",
        "form": contact_form
    }
    if contact_form.is_valid():
        print(contact_form.cleaned_data)
    return render(request, "contact.html", context)


